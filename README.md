## DSD06 - ProjectTaskService

### Deploy

Hướng dẫn deploy với Gitlab-Runner

* Bước 1: Có sẵn một host (EC2, Azure, GoogleCloud, DigitalOcean, ...)
* Bước 2: Upload code lên một project Gitlab
* Bước 3: 
 * Trên host, tiến hành cài đặt gitlab-runner:
   `sudo apt-get update`
   `sudo apt-get install gitlab-runner`
 * Đăng ký một runner: 
   `sudo gitlab-runner register`
 * Tiến hành điền các thông tin đăng ký runner mới (Chú ý phần gitlab-c coordinator URL lấy tại Settings->CI/CD của project gitlab)

* Bước 4: Chạy deploy CI/CD của project. Từ bây giờ, mỗi khi update code sẽ được tự động deploy lên server


### Chạy trên local

* Để chạy project trên local cần tiến hành cài đặt python3 và pip (phụ thuộc hệ điều hành)
* cd src/
* pip install -r ../requirements.txt
* python3 manage.py runserver
* Open `127.0.0.1:8000`
