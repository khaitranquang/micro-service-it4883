#!/bin/sh

install_packages() {
    if ! which python3.6 &>/dev/null; then
        add-apt-repository -y ppa:deadsnakes/ppa
    fi

    apt-get update -qq && apt-get install -y python3.6 python3.6-dev python3-pip libmysqlclient-dev python-dev nginx
    python3.6 -m pip install virtualenv
    echo "[+] Installed packages"
}

make_dir() {
    for path in /var/log/it4883/api/nginx/ /var/log/it4883/api/app/ /var/log/it4883/api/gunicorn/ /etc/it4883/api/ /usr/lib/it4883/api/src;do
        mkdir -p $path
    done
    if [ ! -f /usr/lib/it4883/api/projectenv/bin/python ];
        then python3.6 -m virtualenv /usr/lib/it4883/api/projectenv;
    fi
    echo "[+] Created directories"
}

install_python_libs() {
    /usr/lib/it4883/api/projectenv/bin/pip install --upgrade pip
    /usr/lib/it4883/api/projectenv/bin/pip install -r requirements.txt
    echo "[+] Installed python libraries"
}

copy_source_code() {
    rsync -avr --delete src/ /usr/lib/it4883/api/src/
    echo "[+] Copied source code"
}

config() {
    cp deploy/config/it4883-api.service /lib/systemd/system/
    cp deploy/config/api.group6it4883.tk /etc/it4883/api/api.group6it4883.tk 
        ln -sf /etc/it4883/api/api.group6it4883.tk /etc/nginx/sites-enabled/
    rm -rf /etc/nginx/sites-enabled/default
    sudo /bin/systemctl daemon-reload
    sudo /bin/systemctl enable it4883-api.service
    echo "[+] Configured"
}

create_users() {
    if [ -z "`getent group it4883_api`" ]; then
        addgroup --system it4883_api >/dev/null 2>&1
    fi
    if [ -z "`getent passwd it4883_api`" ]; then
        adduser --system --home /usr/lib/it4883/api/.home --shell /bin/false \
			 --ingroup it4883_api --disabled-password --disabled-login \
			 --gecos "it4883 API account" it4883_api >/dev/null 2>&1
    fi
    echo "[+] Created users"
}

set_permissions() {
    chown -R ubuntu:ubuntu /var/log/it4883/api
    chmod 700 /var/log/it4883/api
    
    chown root:ubuntu /usr/lib/it4883/api
    chmod 750 /etc/it4883/api
    
    chown root:ubuntu /usr/lib/it4883/api
    chmod 750 /usr/lib/it4883/api

    echo "[+] Set permissions"
}

start_services() {
    service it4883-api restart
    service nginx reload
    echo "[+] Started services"
}

install_packages
make_dir
install_python_libs
replace
copy_source_code
config
# create_users
set_permissions
start_services









