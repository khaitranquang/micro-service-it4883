import os
import traceback
import logging.config


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))).replace("server_config", "")


try:
    SECRET_KEY = 'b46cb4v+sj9h_(^0!@1lu$x1r&rhv$j2yg*a8o(t2k#d(9-l3u'
    DEBUG = False
    APPEND_SLASH = False
    WSGI_APPLICATION = 'server_config.wsgi.application'
    ALLOWED_HOSTS = ["*"]

    # Slack Log token
    TOKEN_AUTH_SLACK = "xoxp-647127341297-642057859746-653465330741-98ee2b619aef05fb509318fc12405a62"

    WEB_URL = "http://localhost:3000"

    # AWS S3
    AWS_S3_ACCESS_KEY = "AKIA5AUQGBNRR4BVB7PR"
    AWS_S3_SECRET_KEY = "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"
    AWS_S3_BUCKET = "white-hat"
    AWS_S3_REGION_NAME = "us-east-1"
    AWS_S3_DOMAIN = "https://white-hat.s3.amazonaws.com"

    # My SQL Database
    DATABASES = {
        'default': {
            'ENGINE': "django.db.backends.mysql",
            'NAME': "micro_service_it4883",
            'USER': "it4883",
            'PASSWORD': "bWljcm9fc2VydmljZV9pdDQ4ODM=",
            'HOST': "3.1.20.54",
            'PORT': 3306,
            'CONN_MAX_AGE': 600,
            'OPTIONS': {
                'init_command': 'ALTER DATABASE micro_service_it4883 CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci',
                'charset': 'utf8mb4',  # <--- Use this
            }
        }
    }

    # DATABASES = {
    #     'default': {
    #         'ENGINE': "django.db.backends.mysql",
    #         'NAME': "micro_service_it4883",
    #         'USER': "root",
    #         'PASSWORD': "1234",
    #         'HOST': "127.0.0.1",
    #         'PORT': 3306,
    #         'CONN_MAX_AGE': 600,
    #         'OPTIONS': {
    #             'init_command': 'ALTER DATABASE micro_service_it4883 CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci',
    #             'charset': 'utf8mb4',  # <--- Use this
    #         }
    #     }
    # }

    # Installed apps config
    INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.staticfiles',
        'corsheaders',
        'rest_framework',
        'v1_0'
    ]

    # Authentication Backend class
    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.AllowAllUsersModelBackend'
    ]

    MIDDLEWARE = [
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'shared.middlewares.response_error.ResponseErrorMiddleware',
        # 'shared.middlewares.api_log_middleware.ApiLogMiddleware',
    ]

    ROOT_URLCONF = 'server_config.urls'

    # Here is REST config
    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': [
            # 'shared.authentications.basic_token_auth.BasicTokenAuthentication'
        ],
        'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
        ),
        'EXCEPTION_HANDLER': 'shared.error_responses.custom_exception_handler.custom_exception_handler',
        'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
        'PAGE_SIZE': 10,

        # 'DEFAULT_THROTTLE_CLASSES': (
        #     'rest_framework.throttling.AnonRateThrottle',
        #     'rest_framework.throttling.UserRateThrottle'
        # ),
        # 'DEFAULT_THROTTLE_RATES': {
        #     'anon': '1200/min',
        #     'user': '3000/min'
        # }
    }

    # CORS config - Allow all
    CORS_ORIGIN_ALLOW_ALL = True
    CORS_ALLOW_CREDENTIALS = True
    CORS_EXPOSE_HEADERS = ('location', 'Location')
    CORS_ALLOW_HEADERS = (
        'accept',
        'accept-encoding',
        'authorization',
        'content-type',
        'dnt',
        'origin',
        'user-agent',
        'x-csrftoken',
        'x-requested-with',
        'username', 'password'
    )
    CORS_ALLOW_METHODS = (
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'OPTIONS'
    )

    # Other config
    LANGUAGE_CODE = 'en-us'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True
    STATIC_URL = '/static/'

except Exception as e:
    from shared.log.config import logging_config
    logging.config.dictConfig(logging_config)
    logger = logging.getLogger('customer_service')
    tb = traceback.format_exc()
    logger.critical(tb)
