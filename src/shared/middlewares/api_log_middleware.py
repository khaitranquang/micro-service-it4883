import time

from django.db import connection
import logging

logger = logging.getLogger('stdout_service')


class ApiLogMiddleware(object):
    """
    This middleware will log the number of queries run
    and the total time taken for each request (with a
    status code of 200). It does not currently support
    multi-db setups.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self.get_response(request)
        response = self.process_response(request, response)
        return response

    def process_request(self, request):
        """
        Set Request Start Time to measure time taken to service request.
        """
        request.start_time = time.time()

    def process_response(self, request, response):
        try:
            api_log = request.api_log
            print("API log object: ", api_log)
            api_log.response_code = response.status_code
            api_log.save()

        except Exception as e:
            print("API Log response middleware error: {}".format(e))
        return response

