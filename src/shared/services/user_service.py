import requests

# API Group 5
USER_SERVICE_URL = "https://dsd05-dot-my-test-project-252009.appspot.com"
USER_HEADERS = {
    'Content-Type': 'application/json'
}


class UserService:

    @classmethod
    def get_user_info(cls, user_id):
        url = USER_SERVICE_URL + "/user/getUserInfo?id={}".format(user_id)
        res = requests.get(url=url, headers=USER_HEADERS)
        if res.status_code == 200:
            return res.json()
        return None
