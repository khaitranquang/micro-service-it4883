import logging
from slackclient import SlackClient

from django.conf import settings


class SlackHandler(logging.Handler):
    def emit(self, record):
        try:
            record.exc_info = record.exc_text = None
            content = {'text': self.format(record)}
            # TODO authorize to thread job
            sc = SlackClient(settings.TOKEN_AUTH_SLACK)
            sc.api_call(
                "chat.postMessage",
                channel="#api_log",
                text="```{}```".format(content['text'])
            )
        except:
            self.handleError(record)
