import base64
import traceback
import boto3
from botocore.config import Config

from django.conf import settings

from shared.utils.apps import random_n_digit, url_decode, get_key_s3, get_file_name_extension

UPLOAD_ACTION_TASK_ATTACHMENTS = "task_attachments"

LIST_UPLOAD_ACTION = [UPLOAD_ACTION_TASK_ATTACHMENTS]


class FileUtils:
    """
    This class are file's utils that interact with AWS S3
    """

    def __init__(self, file=None, **metadata):
        self._file = file
        self._metadata = metadata
        self._path = self._generate_path()

    def _generate_path(self):
        action = self._metadata.get("action")

        # This path is temp directory
        if action == UPLOAD_ACTION_TASK_ATTACHMENTS:
            path = "bug-bounty/assets/Temp/user/tasks/{}".format(random_n_digit(n=20)) + "/${filename}"
        else:
            path = ""
        return path

    def gen_presigned_url(self, content_type=None, limit=True):
        """
        This func to create new pre_signed url for client uploader
        :return: pre-signed AWS S3 Url to upload
        """

        fields = {"success_action_status": "201", "acl": "public-read"}
        conditions = [
            {"success_action_status": "201"},
            {"acl": "public-read"}
        ]
        if limit is True:
            action = self._metadata.get("action")
            if action in [UPLOAD_ACTION_TASK_ATTACHMENTS]:
                conditions.append(["content-length-range", 0, 52428800])        # 50MB

        if content_type is not None:
            # Add content type
            fields.update({"Content-Type": content_type})
            conditions.append({"Content-Type": content_type})

        try:
            s3 = boto3.client(
                's3',
                region_name=settings.AWS_S3_REGION_NAME,
                aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
                aws_secret_access_key=settings.AWS_S3_SECRET_KEY,
                config=Config(signature_version='s3v4')
            )

            response = s3.generate_presigned_post(
                Bucket=settings.AWS_S3_BUCKET,
                Key=self._path,
                Fields=fields,
                Conditions=conditions,
                ExpiresIn=300
            )
            return response

        except:
            import logging.config
            from shared.log.config import logging_config
            logging.config.dictConfig(logging_config)
            logger = logging.getLogger('slack_service')
            tb = traceback.format_exc()
            logger.debug(tb)

    @classmethod
    def check_file_exist(cls, key):
        """
        Check file is exist on s3
        :param key: Path to file
        :return: True if file exists
        """
        from botocore.errorfactory import ClientError

        s3 = boto3.client('s3', aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
                          aws_secret_access_key=settings.AWS_S3_SECRET_KEY)
        try:
            s3.head_object(Bucket=settings.AWS_S3_BUCKET, Key=key)
            return True
        except ClientError:
            # Not found
            return False

    @classmethod
    def copy_file(cls, old_path, new_path):
        """
        This method to copy file from old key to new key AWS S3
        :param old_path: old key
        :param new_path: new key
        :return: new key
        """

        try:
            s3 = boto3.resource(
                's3',
                aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
                aws_secret_access_key=settings.AWS_S3_SECRET_KEY
            )
            copy_source = {
                'Bucket': settings.AWS_S3_BUCKET,
                'Key': old_path
            }
            extra_args = {
                'ACL': 'public-read'
            }
            s3.meta.client.copy(copy_source, settings.AWS_S3_BUCKET, new_path, extra_args)
            return new_path
        except:
            import logging.config
            from shared.log.config import logging_config
            logging.config.dictConfig(logging_config)
            logger = logging.getLogger('slack_service')
            tb = traceback.format_exc()
            logger.debug(tb)
            logger.debug("Copy from Temp folder error")

    @classmethod
    def upload_base64(cls, file_base64, content_type, s3_key, acl="public_read"):
        """
        Uploading file (base64 format) to AWS S3
        :param file_base64: base64 string
        :param s3_key: S3 path
        :param content_type:
        :param acl:
        :return:
        """
        try:
            s3 = boto3.client('s3', aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
                              aws_secret_access_key=settings.AWS_S3_SECRET_KEY)
            s3.put_object(Bucket=settings.AWS_S3_BUCKET,
                          Key=s3_key,
                          Body=base64.b64decode(file_base64),
                          ACL=acl,
                          ContentType=content_type)
            return s3_key
        except Exception as e:
            import logging.config
            from shared.log.config import logging_config
            logging.config.dictConfig(logging_config)
            logger = logging.getLogger('customer_service')
            tb = traceback.format_exc()
            logger.debug(tb)

    @classmethod
    def get_s3_url(cls, temp_url, upload_action, **metadata):
        """
        Converting temp url to original cystack CND url
        :param temp_url:
        :param upload_action:
        :return:
        """
        # Decode temp key
        temp_url = url_decode(url=temp_url)
        # Convert url to key path
        temp_url = get_key_s3(url=temp_url)

        # If not temp path (origin path) => Convert to temp path
        if "Temp" not in temp_url:
            elements = temp_url.split("/")
            file_name = elements[len(elements) - 1]

            if upload_action == UPLOAD_ACTION_TASK_ATTACHMENTS:
                temp_url = "bug-bounty/assets/Temp/user/tasks/{}".format(file_name)

        # Get original path
        if upload_action == UPLOAD_ACTION_TASK_ATTACHMENTS:
            file_s3_url = temp_url.replace("/Temp", "").replace(" ", "_")
        else:
            file_s3_url = temp_url.replace("bug-bounty/assets/Temp/", "").replace(" ", "_")

        # Check is in temp url
        is_file_in_temp = cls.check_file_exist(key=temp_url)
        # Check file is in main url
        is_file_in_main = cls.check_file_exist(key=file_s3_url)

        print(is_file_in_temp, is_file_in_main)

        # If check file is exist in temp => Copy file to original directory
        if is_file_in_temp is True:
            # If main directory has the same file name => Rename
            if is_file_in_main is True:
                old_name, extension = get_file_name_extension(key=file_s3_url)
                new_name = "{}_{}{}".format(old_name, random_n_digit(n=20), extension)
                file_s3_url = file_s3_url.replace("{}{}".format(old_name, extension), new_name)

            new_path = cls.copy_file(old_path=temp_url, new_path=file_s3_url)

            return settings.AWS_S3_DOMAIN + "/" + new_path

        # Else not exist in temp => Check file in original
        else:
            if cls.check_file_exist(key=file_s3_url) is True:
                return settings.AWS_S3_DOMAIN + "/" + file_s3_url
        return ""

    @classmethod
    def delete_file(cls, url):
        if not url.startswith(settings.AWS_S3_DOMAIN):
            return
        try:
            key = get_key_s3(url=url)
            s3 = boto3.client('s3', aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
                              aws_secret_access_key=settings.AWS_S3_SECRET_KEY)
            s3.delete_object(Bucket=settings.AWS_S3_BUCKET,
                             Key=key)
        except Exception as e:
            import logging.config
            from shared.log.config import logging_config
            logging.config.dictConfig(logging_config)
            logger = logging.getLogger('customer_service')
            tb = traceback.format_exc()
            logger.debug(tb)
