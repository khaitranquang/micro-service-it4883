from rest_framework import viewsets

# from shared.utils.apps import now
# from v1_0.models.tokens.tokens import Token


class MicroServiceViewSet(viewsets.GenericViewSet):
    def initial(self, request, *args, **kwargs):
        super(MicroServiceViewSet, self).initial(request, *args, **kwargs)

        # Delete all expired tokens
        # Token.objects.filter(expired_time__lt=now()).delete()
