NOTIFICATION_ADD_MEMBER = "Bạn đã được thêm vào task {}"
NOTIFICATION_DELETE_MEMBER = "Bạn đã bị xóa khỏi task {}"

NOTIFICATION_TASK_ON_REVIEW = "Thành viên của task {} đã yêu cầu phê duyệt hoàn thành công việc"
NOTIFICATION_TASK_COMPLETED = "Task {} đã được đánh dấu hoàn thành"
NOTIFICATION_TASK_UPDATE = "Task {} vừa được cập nhật thông tin"
