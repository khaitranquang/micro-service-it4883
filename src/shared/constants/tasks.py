TASK_STATUS_PROCESSING = "processing"
TASK_STATUS_ON_REVIEW = "on_review"
TASK_STATUS_COMPLETED = "completed"

TASK_LABEL_LOW = "LOW"
TASK_LABEL_MEDIUM = "MEDIUM"
TASK_LABEL_HIGH = "HIGH"

TASK_LEVEL_EASY = "EASY"
TASK_LEVEL_MEDIUM = "MEDIUM"
TASK_LEVEL_DIFFICULT = "DIFFICULT"
