import difflib
import os
import socket
import pytz
import random
import string
import hashlib
import requests
import urllib.parse
from urllib.parse import urlparse
from datetime import datetime
from PIL import Image
from io import BytesIO

from django.conf import settings
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


def random_n_digit(n):
    """
    Create random string with n character(s)
    :param n: Number of character(s)
    :return: Random string
    """
    return ''.join([random.choice(string.digits + string.ascii_lowercase) for _ in range(n)])


def now():
    """
    Get timestamp now
    :return: now (int)
    """
    time_now = datetime.now(tz=pytz.UTC)
    timestamp_now = time_now.timestamp()
    return int(timestamp_now)


def diff_list(new_list, old_list):
    return [x for x in new_list if x not in old_list]


def get_ip_by_request(request):
    ip_address = ''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', '')
    if x_forwarded_for and ',' not in x_forwarded_for:
        if is_valid_ip(x_forwarded_for):
            ip_address = x_forwarded_for.strip()
    else:
        ips = [ip.strip() for ip in x_forwarded_for.split(',')]
        for ip in ips:
            if not is_valid_ip(ip):
                continue
            else:
                ip_address = ip
                break
    if not ip_address:
        x_real_ip = request.META.get('HTTP_X_REAL_IP', '')
        if x_real_ip and is_valid_ip(x_real_ip):
            ip_address = x_real_ip.strip()

    if not ip_address:
        remote_addr = request.META.get('REMOTE_ADDR', '')
        if remote_addr and is_valid_ip(remote_addr):
            ip_address = remote_addr.strip()

    if not ip_address:
        ip_address = '127.0.0.1'
    return ip_address


def is_valid_ip(ip_address):
    return is_valid_ipv4_address(address=ip_address) or is_valid_ipv6_address(address=ip_address)


def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address.strip())
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False
    return True


def is_valid_ipv6_address(address):
    try:
        socket.inet_pton(socket.AF_INET6, address.strip())
        return True
    except socket.error:  # not a valid address
        return False


def url_decode(url):
    """
    Decode url utf-8
    :param url:
    :return:
    """
    url = url.replace("+", "%20")
    url = urllib.parse.unquote(url)
    return url


def get_key_s3(url):
    """
    Get key from s3 url
    :param url: s3 or WH CDN url
    :return: key s3
    """
    if "https://{}.s3.amazonaws.com/".format(settings.AWS_S3_BUCKET) in url:
        url = url.replace("https://{}.s3.amazonaws.com/".format(settings.AWS_S3_BUCKET), "")

    return url


def get_file_name_extension(key):
    """
    Get file name and extension from key s3
    :param key: S3 Key
    :return: (file_name, extension)
    """
    base = os.path.basename(key)
    file_name, extension = os.path.splitext(base)
    return file_name, extension


def check_url(url):
    """
    Check string is valid url
    :param url: String need check
    :return: True if valid
    """
    url_validator = URLValidator(schemes=['http', 'https'])
    try:
        url_validator(url)
    except ValidationError as e:
        return False
