import requests

from shared.notifications.utils import BackgroundThread


BASE_NOTIFICATION_API = "https://api-ptpmpt-18.herokuapp.com"
API_NOTIFICATION_LOGIN_URL = BASE_NOTIFICATION_API + "/api/auth/login"
API_NOTIFICATION_CREATE = BASE_NOTIFICATION_API + "/api/notification/createNotification"
API_USERNAME = "group06"
API_PASSWORD = "group06"
API_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVkZGU4MDk0MzZmZDZkMDAxNzBkNTU0OSIsInVzZXJuYW1lIjoiZ3JvdXAwNiIsInBhc3N3b3JkIjoiJDJhJDEwJFgwdEJQajdRRHd4R1IzLnB6U2czNk9MU1VtaTJZL3hjMllrVy9aa2tXRGo5dVZVTU5MeTl1IiwiX192IjowfSwiaWF0IjoxNTc0ODYzMDQ5LCJleHAiOjE1NzQ4NjY2NDl9.4rPbq31ekZdE9pruCCgGHn5DD9XEyw_OvEhE-5ZmUlI"


class Notification:
    def __init__(self):
        self._token = API_TOKEN

    def __create_notification_api(self, title, content, user_ids):
        data_send = {
            "title": title,
            "content": content,
            "users": list(user_ids)
        }
        headers = {
            "Authorization": "Bearer " + self._token,
            "Content-Type": "application/json"
        }
        res = requests.post(url=API_NOTIFICATION_CREATE, headers=headers, json=data_send)
        return res

    def create(self, **kwargs):
        title = kwargs.get("title")
        content = kwargs.get("content")
        user_ids = kwargs.get("user_ids")
        res = self.__create_notification_api(title=title, content=content, user_ids=user_ids)
        if res.status_code >= 400:
            # Get token and re-send
            auth_data = {
                "username": API_USERNAME,
                "password": API_PASSWORD
            }
            auth_header = {
                "Content-Type": "application/json"
            }
            res_auth = requests.post(url=API_NOTIFICATION_LOGIN_URL, json=auth_data, headers=auth_header)
            if res_auth.status_code == 200:
                self._token = res_auth.json().get("token")
                self.__create_notification_api(title=title, content=content, user_ids=user_ids)

    def send(self, **kwargs):
        BackgroundThread(task=self.create, **kwargs)