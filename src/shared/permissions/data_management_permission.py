from shared.permissions.base_permission import MSPermission
from shared.permissions.utils import *


class DataManagementPermission(MSPermission):
    def has_permission(self, request, view):

        return super(DataManagementPermission, self).has_permission(request, view)

    def has_object_permission(self, request, view, obj):

        return super(DataManagementPermission, self).has_object_permission(request, view, obj)