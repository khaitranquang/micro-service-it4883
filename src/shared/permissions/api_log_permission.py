from shared.permissions.base_permission import MSPermission
from shared.permissions.utils import *


class ApiLogPermission(MSPermission):
    def has_permission(self, request, view):

        return super(ApiLogPermission, self).has_permission(request, view)

    def has_object_permission(self, request, view, obj):

        return super(ApiLogPermission, self).has_object_permission(request, view, obj)