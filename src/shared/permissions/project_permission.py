from shared.permissions.base_permission import MSPermission
from shared.permissions.utils import *


class ProjectPermisison(MSPermission):
    def has_permission(self, request, view):

        return super(ProjectPermisison, self).has_permission(request, view)

    def has_object_permission(self, request, view, obj):

        return super(ProjectPermisison, self).has_object_permission(request, view, obj)