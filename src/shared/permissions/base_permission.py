from rest_framework.permissions import BasePermission


class MSPermission(BasePermission):
    def has_permission(self, request, view):
        return super(MSPermission, self).has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        return super(MSPermission, self).has_object_permission(request, view, obj)

    def is_auth(self, requuest):
        return requuest.user and requuest.user.is_authenticated and requuest.auth