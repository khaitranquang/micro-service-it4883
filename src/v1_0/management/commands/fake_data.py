import ast
import json
import time
import random


from django.core.management import BaseCommand
from django.conf import settings

from v1_0.models import Project, Task, TaskMember


class Command(BaseCommand):
    def handle(self, *args, **options):
        project = Project.objects.get(id="1664a77a-9c82-46af-83fe-b05b5e2e4bf8")

        task_name = "Hoàn thành"
        task_desc = "Hoàn thành hồ sơ thủ tục để nhân viên trúng tuyển có thể đến làm việc"
        created_time = 1577318400
        updated_time = 1577318400
        status = "processing"
        deadline = 1577491200
        completed_time = None
        percent_completed = 0
        level = 1

        new_task = Task.objects.create(
            project=project,
            name=task_name,
            description=task_desc,
            created_time=created_time,
            updated_time=updated_time,
            status=status,
            deadline=deadline,
            completed_time=completed_time,
            percent_complete=percent_completed,
            level=level
        )

        # task = Task.objects.get(id='de436658-49d8-4471-8f67-9c0ea1387d53')
        task = new_task
        user_ids = ["6280445314465792", "6253207772725248", "6244517946589184", "6225279680774144", "6213393157455872"]
        number_member = random.randint(1, 5)
        number_members = range(number_member)
        for index in number_members:
            new_member = TaskMember.objects.create(
                user_id=user_ids[index],
                access_time=task.created_time,
                task=task
            )

