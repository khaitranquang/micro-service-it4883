user_ids = [
    "5097662860754944",
    "5134733327466496",
    "5141955650519040",
    "5183170693562368",
    "5201579628036096",
    "5272127519326208",
    "5592497719869440",
    "5796125139271680",
    "5810205719789568",
    "5914844578447360",
    "6035135371673600",
    "6323275600756736",
    "6329415994703872",
    "6348165540216832",
    "6468672021331968",
    "6558115822567424",
    "6718397626712064"
]

import random

from django.core.management import BaseCommand
from django.conf import settings

from v1_0.models import Project, Task, TaskMember


class Command(BaseCommand):


    def handle(self, *args, **options):
        members = TaskMember.objects.all()
        for member in members:
            print(member.id)
            while True:
                user_id = user_ids[random.randint(0, 16)]
                try:
                    member.user_id = user_id
                    member.save()
                    break
                except:
                    print("Unique together: {} {}", user_id, member.task_id)
                    continue

