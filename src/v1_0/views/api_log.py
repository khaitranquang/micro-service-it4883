from rest_framework.response import Response
from rest_framework.exceptions import NotFound

from shared.permissions.api_log_permission import ApiLogPermission
from v1_0.models.api_log import ApiLog
from v1_0.serializers.api_log import ApiLogSerializer
from v1_0.views.app_general_view import MSBaseView


class ApiLogViewSet(MSBaseView):
    permission_classes = (ApiLogPermission, )
    http_method_names = ['head', 'options', 'get']
    serializer_class = ApiLogSerializer

    def get_queryset(self):
        _all_logs = ApiLog.objects.all().order_by('-created_time')

        type_param = self.request.query_params.get("type", None)
        method_param = self.request.query_params.get("method", None)
        response_code_param = self.request.query_params.get("response_code", None)
        from_param = self.check_int_param(self.request.query_params.get("from", None))
        to_param = self.check_int_param(self.request.query_params.get("to", None))

        if type_param is not None:
            _all_logs = _all_logs.filter(type=type_param)
        if method_param is not None:
            _all_logs = _all_logs.filter(method=method_param)
        if response_code_param is not None:
            _all_logs = _all_logs.filter(response_code=response_code_param)
        if from_param is not None:
            _all_logs = _all_logs.filter(created_time__gte=from_param)
        if to_param is not None:
            _all_logs = _all_logs.filter(created_time__lte=to_param)

        return _all_logs

    def list(self, request, *args, **kwargs):
        _paging = self.request.query_params.get("paging", None)
        if (_paging is not None) and (_paging == "0"):
            self.pagination_class = None
        return super(ApiLogViewSet, self).list(request, *args, **kwargs)
