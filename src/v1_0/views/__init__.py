from v1_0.views.projects import ProjectViewSet
from v1_0.views.tasks import TaskViewSet
from v1_0.views.task_members import TaskMemberViewSet
from v1_0.views.attachments import AttachmentViewSet

from v1_0.views.api_log import ApiLogViewSet
from v1_0.views.statistics import StatisticViewSet

from v1_0.views.data_management import DataManagementViewSet
from v1_0.views.project_types import ProjectTypeViewSet
