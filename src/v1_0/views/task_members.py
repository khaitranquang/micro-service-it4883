from django.db.models.functions import Lower
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ValidationError

from shared.constants.notifications import NOTIFICATION_ADD_MEMBER, NOTIFICATION_DELETE_MEMBER
from shared.notifications.notification import Notification
from shared.permissions.task_member_permission import TaskMemberPermission
from v1_0.models.tasks import Task
from v1_0.models.task_members import TaskMember
from v1_0.serializers.members import MemberSerializer
from v1_0.views.app_general_view import MSBaseView


class TaskMemberViewSet(MSBaseView):
    permission_classes = (TaskMemberPermission,)
    lookup_value_regex = r'[0-9]+'
    http_method_names = ['head', 'options', 'get', 'post', 'put', 'delete']
    serializer_class = MemberSerializer

    def get_serializer_class(self):
        return super(TaskMemberViewSet, self).get_serializer_class()

    def get_object(self):
        print(self.kwargs.get("pk"), self.kwargs.get("task_id"))
        try:
            _member = TaskMember.objects.get(user_id=self.kwargs.get("pk"), task_id=self.kwargs.get("task_id"))
            self.check_object_permissions(request=self.request, obj=_member)
            return _member
        except TaskMember.DoesNotExist:
            print("NOT FOUNDDDD")
            raise NotFound

    def get_task(self):
        try:
            _task = Task.objects.get(id=self.kwargs.get("task_id"))
            return _task
        except Task.DoesNotExist:
            raise NotFound

    def get_queryset(self):
        _all_members = TaskMember.objects.filter(task_id=self.kwargs.get("task_id")).order_by('-access_time')

        return _all_members

    def list(self, request, *args, **kwargs):
        task = self.get_task()
        _paging = self.request.query_params.get("paging", None)
        if (_paging is not None) and (_paging == "0"):
            self.pagination_class = None
        return super(TaskMemberViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        task = self.get_task()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_id = serializer.validated_data.get("user_id")
        if TaskMember.objects.filter(user_id=user_id, task=task).exists():
            raise ValidationError(detail={"user_id": "This member user id was existed"})

        member = serializer.save(**{"task": task})
        Notification().send(**{
            "title": NOTIFICATION_ADD_MEMBER.format(task.name),
            "content": NOTIFICATION_ADD_MEMBER.format(task.name),
            "user_ids": [user_id]
        })
        return Response(status=201, data={"id": member.id})

    # def retrieve(self, request, *args, **kwargs):
    #     return super(TaskMemberViewSet, self).retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        member = self.get_object()
        user_id = member.user_id
        task = member.task
        member.delete()
        Notification().send(**{
            "title": NOTIFICATION_DELETE_MEMBER.format(task.name),
            "content": NOTIFICATION_DELETE_MEMBER.format(task.name),
            "user_ids": [user_id]
        })
        return Response(status=204)
