from rest_framework import mixins

from shared.views.basic_view import MicroServiceViewSet
from shared.utils.apps import get_ip_by_request, now
from v1_0.models.api_log import ApiLog


class AppViewSet(MicroServiceViewSet):
    def initial(self, request, *args, **kwargs):
        method = request.method
        if method.lower() not in ['options']:
            try:
                # Create API log object
                simple_path = request.path
                api_type = "unknown"
                object_id = None
                if "/v1/logs" in simple_path:
                    return super(AppViewSet, self).initial(request, *args, **kwargs)
                if "projects" in simple_path:
                    object_id = kwargs.get("pk")
                    if "members" in simple_path:
                        api_type = "members"
                    else:
                        api_type = "projects"
                if "tasks" in simple_path:
                    object_id = kwargs.get("pk")
                    if "members" in simple_path:
                        api_type = "members"
                    else:
                        api_type = "tasks"
                if "attachments" in simple_path:
                    object_id = kwargs.get("pk")
                    api_type = "attachments"
                if "statistic" in simple_path:
                    api_type = "statistics"
                    if "users" in simple_path:
                        object_id = kwargs.get("user_id")

                ip = get_ip_by_request(request=request)

                data_send = request.data
                path = request.get_full_path()

                new_api_log = ApiLog.create(**{
                    "ip": ip,
                    "object_id": object_id,
                    "method": method,
                    "data_send": data_send,
                    "path": path,
                    "type": api_type
                })
                request.api_log = new_api_log

            except Exception as e:
                print(e)
                pass

        # Delete all log expired (30 days)
        expired_time = now() - 30 * 86400
        ApiLog.objects.filter(created_time__lte=expired_time).delete()

        return super(AppViewSet, self).initial(request, *args, **kwargs)
    
    def dispatch(self, request, *args, **kwargs):
        return super(AppViewSet, self).dispatch(request, *args, **kwargs)

    def finalize_response(self, request, response, *args, **kwargs):
        try:
            api_log = request.api_log
            api_log.response_code = response.status_code
            api_log.save()
        except Exception as e:
            print("API Log finalize_response error: {}".format(e))
        return super(AppViewSet, self).finalize_response(request, response, *args, **kwargs)


class MSBaseView(mixins.CreateModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 mixins.DestroyModelMixin,
                 mixins.ListModelMixin, AppViewSet):
    def get_serializer_context(self):
        context = super(MSBaseView, self).get_serializer_context()
        user = self.request.user
        if user.is_anonymous:
            user = None
        context["request"].user = user
        return context

    def get_serializer(self, *args, **kwargs):
        return super(MSBaseView, self).get_serializer(*args, **kwargs)

    def check_int_param(self, param=None):
        if param is None:
            return None
        try:
            param_int = int(param)
        except ValueError:
            param_int = None
        return None if param_int is None else param