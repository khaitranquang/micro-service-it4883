from django.db.models import Q, F
from django.db.models.functions import Lower
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework.decorators import action
from rest_framework.settings import api_settings

from shared.constants.members import *
from shared.constants.projects import *
from shared.permissions.project_permission import ProjectPermisison
from shared.utils.apps import now
from v1_0.models.projects import Project
from v1_0.models.project_type import ProjectType
from v1_0.serializers.projects import ProjectSerializer, UpdateProjectSerializer, UserProjectSerializer
from v1_0.views.app_general_view import MSBaseView


class ProjectViewSet(MSBaseView):
    permission_classes = (ProjectPermisison, )
    lookup_value_regex = r'[0-9a-z\-]+'
    http_method_names = ['head', 'options', 'get', 'post', 'put', 'delete']
    serializer_class = ProjectSerializer

    def get_serializer_class(self):
        if self.action == "update":
            self.serializer_class = UpdateProjectSerializer
        if self.action == "user_projects":
            self.serializer_class = UserProjectSerializer
        return super(ProjectViewSet, self).get_serializer_class()

    def get_object(self):
        try:
            _project = Project.objects.get(id=self.kwargs.get("pk"))
            self.check_object_permissions(request=self.request, obj=_project)
            return _project
        except Project.DoesNotExist:
            raise NotFound

    def get_queryset(self):
        _all_projects = Project.objects.all().order_by('-created_time')

        # Get filter params
        created_by_param = self.request.query_params.get("created_by", None)
        q_param = self.request.query_params.get("q", None)
        created_from = self.check_int_param(param=self.request.query_params.get("created_from", None))
        created_to = self.check_int_param(param=self.request.query_params.get("created_to", None))
        updated_from = self.check_int_param(param=self.request.query_params.get("updated_from", None))
        updated_to = self.check_int_param(param=self.request.query_params.get("updated_to", None))
        project_type = self.check_int_param(param=self.request.query_params.get("project_type", None))

        # Filter by user created
        if created_by_param is not None:
            _all_projects = _all_projects.filter(created_by=created_by_param)

        # Filter by project type
        if project_type is not None:
            _all_projects = _all_projects.filter(project_type__id=project_type)

        # Filter by name
        if q_param is not None:
            _all_projects = _all_projects.annotate(lower_name=Lower('name')).filter(
                lower_name__contains=q_param.lower()
            )
        # Filter by created time
        if created_from is not None:
            _all_projects = _all_projects.filter(created_time__gte=created_from)
        if created_to is not None:
            _all_projects = _all_projects.filter(created_time__lte=created_to)
        # Filter by updated time
        if updated_from is not None:
            _all_projects = _all_projects.filter(updated_time__gte=updated_from)
        if updated_to is not None:
            _all_projects = _all_projects.filter(updated_time__lte=updated_to)

        return _all_projects

    def list(self, request, *args, **kwargs):
        """
        This API Endpoint to get list projects: filter by filter params
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        # Get pagination class
        _paging = self.request.query_params.get("paging", None)
        if (_paging is not None) and (_paging == "0"):
            self.pagination_class = None
        return super(ProjectViewSet, self).list(request, *args, **kwargs)

    @action(methods=["get"], detail=False)
    def user_projects(self, request, *args, **kwargs):
        """
        This API to get list project of user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user_id = self.kwargs.get("user_id")
        _paging = self.request.query_params.get("paging", None)

        projects_qs = self.get_queryset()
        projects_qs = projects_qs.filter(Q(tasks__members__user_id=user_id) | Q(created_by=user_id)).distinct()

        if (_paging is not None) and (_paging == "0"):
            serializer = UserProjectSerializer(projects_qs, many=True, context={'user_id': user_id})
            return Response(status=200, data={"count": projects_qs.count(), "results": serializer.data})

        pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
        paginator = pagination_class()
        page = paginator.paginate_queryset(projects_qs, request)
        serializer = UserProjectSerializer(page, many=True, context={'user_id': user_id})
        results_data = serializer.data
        return Response(status=200, data={
            "count": len(results_data),
            "results": results_data
        })

    def create(self, request, *args, **kwargs):
        """
        This API Endpoint to user creates new project)
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        project = serializer.save()
        return Response(status=201, data={"id": project.id})

    def retrieve(self, request, *args, **kwargs):
        """
        This API Endpoint to retrieve detail program
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        if kwargs.get("pk") == "statistics":
            return self.statistics(request, *args, **kwargs)
        return super(ProjectViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        """
        This API to update description and deadline of project
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        project = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.save()
        _name = validated_data.get("name")
        _description = validated_data.get("description", "")
        _deadline = validated_data.get("deadline", None)
        _status = validated_data.get("status", None)
        _technique_index = validated_data.get("technique_index", None)
        _project_type = validated_data.get("project_type", None)

        project.name = _name
        project.description = _description
        if _deadline is not None:
            project.deadline = None if _deadline == 0 else _deadline
        if _status is not None and project.status != _status:
            project.status = _status
            if _status == PROJECT_STATUS_PROCESSING:
                project.completed_time = None
            elif _status == PROJECT_STATUS_COMPLETED:
                project.completed_time = now()
        if _technique_index is not None:
            project.technique_index = _technique_index
        if _project_type is not None:
            try:
                project_type_obj = ProjectType.objects.get(id=_project_type)
                project.project_type = project_type_obj
            except ProjectType.DoesNotExist:
                pass
        project.updated_time = now()
        project.save()
        return Response(status=200, data={"success": True})

    @action(methods=["get"], detail=True)
    def members(self, request, *args, **kwargs):
        """
        This API to get list members of project
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        project = self.get_object()
        members = [project.created_by]
        tasks = project.tasks.prefetch_related('members').all()
        for task in tasks:
            task_members = task.members.values_list('user_id', flat=True)
            for user_id in list(task_members):
                if user_id not in members:
                    members.append(user_id)

        members = list(set(members))
        results = []
        for user_id in members:
            role = MEMBER_ROLE_OWNER if user_id == project.created_by else MEMBER_ROLE_MEMBER
            results.append({"user_id": user_id, "role": role})
        return Response(status=200, data={
            "count": len(results),
            "results": results
        })

    @action(methods=["get"], detail=True)
    def statistics(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        completed = queryset.filter(status=PROJECT_STATUS_COMPLETED).count()
        processing = queryset.filter(status=PROJECT_STATUS_PROCESSING).count()
        completed_not_expired = queryset.filter(
            Q(status=PROJECT_STATUS_COMPLETED, deadline__isnull=True) | Q(status=PROJECT_STATUS_COMPLETED, completed_time__lte=F('deadline'))
        ).count()
        completed_expired = completed - completed_not_expired
        processing_not_expired = queryset.filter(
            Q(status=PROJECT_STATUS_PROCESSING, deadline__isnull=True) | Q(status=PROJECT_STATUS_PROCESSING, deadline__gte=now())
        ).count()
        processing_expired = processing - processing_not_expired


        return Response(status=200, data={
            "completed_not_expired": completed_not_expired,
            "completed_expired": completed_expired,
            "processing_not_expired": processing_not_expired,
            'processing_expired': processing_expired
        })

        return Response(status=200, data={
            PROJECT_STATUS_PROCESSING: queryset.filter(status=PROJECT_STATUS_PROCESSING).count(),
            PROJECT_STATUS_COMPLETED: queryset.filter(status=PROJECT_STATUS_COMPLETED).count(),
            "expired": queryset.filter(
                Q(status__in=[PROJECT_STATUS_PROCESSING], deadline__lt=now()) |
                Q(status=PROJECT_STATUS_COMPLETED, completed_time__isnull=False, completed_time__gt=F('deadline'))
            ).count()
        })