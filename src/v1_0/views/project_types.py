from rest_framework.response import Response

from shared.permissions.project_type_permission import ProjectTypePermission
from v1_0.serializers.project_types import ProjectTypeSerializer
from v1_0.models.project_type import ProjectType
from v1_0.views.app_general_view import MSBaseView


class ProjectTypeViewSet(MSBaseView):
    permission_classes = (ProjectTypePermission, )
    lookup_value_regex = r'[0-9]+'
    http_method_names = ['head', 'options', 'get']
    serializer_class = ProjectTypeSerializer

    def get_serializer_class(self):
        return super(ProjectTypeViewSet, self).get_serializer_class()

    def get_queryset(self):
        all_project_types = ProjectType.objects.all().order_by('id')
        return all_project_types

    def list(self, request, *args, **kwargs):
        self.pagination_class = None
        return super(ProjectTypeViewSet, self).list(request, *args, **kwargs)

