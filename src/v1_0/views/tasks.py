from django.db.models import Q, F
from django.db.models.functions import Lower
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, PermissionDenied, ValidationError
from rest_framework.decorators import action

from shared.constants.notifications import  NOTIFICATION_TASK_ON_REVIEW, NOTIFICATION_TASK_COMPLETED, NOTIFICATION_TASK_UPDATE
from shared.notifications.notification import Notification
from shared.permissions.task_permission import TaskPermission
from shared.utils.apps import now
from shared.constants.tasks import *
from v1_0.models.tasks import Task
from v1_0.serializers.tasks import TaskSerializer, UpdateTaskSerializer
from v1_0.views.app_general_view import MSBaseView


class TaskViewSet(MSBaseView):
    permission_classes = (TaskPermission, )
    lookup_value_regex = r'[0-9a-z\-]+'
    http_method_names = ['head', 'options', 'get', 'post', 'put', 'delete']
    serializer_class = TaskSerializer

    def get_serializer_class(self):
        if self.action == "update":
            self.serializer_class = UpdateTaskSerializer
        return super(TaskViewSet, self).get_serializer_class()

    def get_object(self):
        try:
            _task = Task.objects.get(id=self.kwargs.get("pk"))
            self.check_object_permissions(request=self.request, obj=_task)
            return _task
        except Task.DoesNotExist:
            raise NotFound

    def get_queryset(self):
        _all_tasks = Task.objects.all().order_by('-created_time')

        # Get filter params
        project_param = self.request.query_params.get("project_id", None)
        q_param = self.request.query_params.get("q", None)
        status_param = self.request.query_params.get("status", None)
        created_from = self.check_int_param(param=self.request.query_params.get("created_from", None))
        created_to = self.check_int_param(param=self.request.query_params.get("created_to", None))
        label_param = self.request.query_params.get("label", None)
        level_param = self.check_int_param(self.request.query_params.get("level", None))
        is_expired_param = self.request.query_params.get("is_expired", None)
        user_id_param = self.request.query_params.get("user_id", None)
        updated_from = self.check_int_param(param=self.request.query_params.get("updated_from", None))
        updated_to = self.check_int_param(param=self.request.query_params.get("updated_to", None))

        # Filter by project id
        if project_param is not None:
            _all_tasks = _all_tasks.filter(project_id=project_param)
        # Filter by name
        if q_param is not None:
            _all_tasks = _all_tasks.annotate(lower_name=Lower('name')).filter(
                lower_name__contains=q_param.lower()
            )
        # Filter by created time
        if created_from is not None:
            _all_tasks = _all_tasks.filter(created_time__gte=created_from)
        if created_to is not None:
            _all_tasks = _all_tasks.filter(created_time__lte=created_to)
        # Filter by status
        if status_param is not None:
            _all_tasks = _all_tasks.filter(status=status_param)
        # Filter by label
        if label_param is not None:
            _all_tasks = _all_tasks.filter(label=label_param)
        # Filter by level
        if level_param is not None:

            _all_tasks = _all_tasks.filter(level=level_param)
        # Filter expired tasks
        if is_expired_param is not None:
            if is_expired_param == "1":
                _all_tasks = _all_tasks.filter(deadline__gt=now()).exclude(status=TASK_STATUS_COMPLETED)
            if is_expired_param == "0":
                _all_tasks = _all_tasks.filter(deadline__lt=now()).exclude(status=TASK_STATUS_COMPLETED)
        # Filter by updated time
        if updated_from is not None:
            _all_tasks = _all_tasks.filter(updated_time__gte=updated_from)
        if updated_to is not None:
            _all_tasks = _all_tasks.filter(updated_time__lte=updated_to)

        # Filter by user id
        if user_id_param is not None:
            _all_tasks = _all_tasks.filter(members__user_id=user_id_param)

        return _all_tasks

    def list(self, request, *args, **kwargs):
        _paging = self.request.query_params.get("paging", None)
        if (_paging is not None) and (_paging == "0"):
            self.pagination_class = None
        return super(TaskViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = serializer.save()
        return Response(status=201, data={"id": task.id})

    def retrieve(self, request, *args, **kwargs):
        if kwargs.get("pk") == "statistics":
            return self.statistics(request, *args, **kwargs)
        return super(TaskViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        task = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.save()
        _name = validated_data.get("name", None)
        _status = validated_data.get("status", None)
        _label = validated_data.get("label", None)
        _level = validated_data.get("level", None)
        _description = validated_data.get("description", None)
        _deadline = validated_data.get("deadline", None)
        _percent_complete = validated_data.get("percent_complete", None)

        if _name is not None:
            task.name = _name
        if _description is not None:
            task.description = _description
        # if _status is not None and (_status != task.status):
        #     if _status == TASK_STATUS_COMPLETED:
        #         task.completed_time = now()
        #     elif _status == TASK_STATUS_PROCESSING:
        #         task.completed_time = None
        #     task.status = _status
        if _label is not None:
            task.label = _label
        if _level is not None:
            task.level = _level
        if _deadline is not None:
            task.deadline = _deadline
        if _percent_complete is not None:
            task.percent_complete = _percent_complete
            if _percent_complete == 100:
                task.status = TASK_STATUS_ON_REVIEW

            # if _percent_complete == 100:
            #     task.status = TASK_STATUS_COMPLETED
            #     task.completed_time = now()
            else:
                task.status = TASK_STATUS_PROCESSING
                task.completed_time = None

        task.updated_time = now()
        task.save()
        member_user_ids = task.members.all().values_list('user_id', flat=True)
        Notification().send(**{
            "title": NOTIFICATION_TASK_UPDATE.format(task.name),
            "content": NOTIFICATION_TASK_UPDATE.format(task.name),
            "user_ids": list(member_user_ids)
        })

        return Response(status=200, data={"success": True})

    @action(methods=["post"], detail=True)
    def on_review(self, request, *args, **kwargs):
        user_id = request.data.get("user_id", None)
        if user_id is None:
            raise PermissionDenied
        task = self.get_object()
        members = task.members.filter(user_id=user_id)
        if not members.exists():
            raise PermissionDenied
        task.status = TASK_STATUS_ON_REVIEW
        task.save()
        Notification().send(**{
            "title": NOTIFICATION_TASK_ON_REVIEW.format(task.name),
            "content": NOTIFICATION_TASK_ON_REVIEW.format(task.name),
            "user_ids": [task.project.created_by]
        })
        return Response(status=200, data={"success": True})

    @action(methods=["post"], detail=True)
    def completed(self, request, *args, **kwargs):
        user_id = request.data.get("user_id", None)
        completed = request.data.get("completed", None)
        if completed is None:
            raise ValidationError(detail={"completed": ["This field is required"]})
        if user_id is None:
            raise PermissionDenied
        task = self.get_object()
        user_ids = task.members.all().values_list('user_id', flat=True)
        if task.project.created_by != str(user_id):
            raise PermissionDenied
        if completed is True:
            task.status = TASK_STATUS_COMPLETED
            task.completed_time = now()
            Notification().send(**{
                "title": NOTIFICATION_TASK_COMPLETED.format(task.name),
                "content": NOTIFICATION_TASK_COMPLETED.format(task.name),
                "user_ids": list(user_ids)
            })
        else:
            task.status = TASK_STATUS_PROCESSING
        task.save()
        return Response(status=200, data={"success": True})

    @action(methods=["get"], detail=False)
    def statistics(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        completed = queryset.filter(status=TASK_STATUS_COMPLETED).count()
        processing = queryset.filter(status__in=[TASK_STATUS_PROCESSING, TASK_STATUS_ON_REVIEW]).count()
        completed_not_expired = queryset.filter(
            Q(status=TASK_STATUS_COMPLETED, deadline__isnull=True) | Q(status=TASK_STATUS_COMPLETED, completed_time__lte=F('deadline'))
        ).count()
        completed_expired = completed - completed_not_expired
        processing_not_expired = queryset.filter(
            Q(status__in=[TASK_STATUS_PROCESSING, TASK_STATUS_ON_REVIEW], deadline__isnull=True) | Q(status__in=[TASK_STATUS_PROCESSING, TASK_STATUS_ON_REVIEW], deadline__gte=now())
        ).count()
        processing_expired = processing - processing_not_expired

        return Response(status=200, data={
            "completed_not_expired": completed_not_expired,
            "completed_expired": completed_expired,
            "processing_not_expired": processing_not_expired,
            'processing_expired': processing_expired
        })

        # return Response(status=200, data={
        #     TASK_STATUS_COMPLETED: queryset.filter(status=TASK_STATUS_COMPLETED).count(),
        #     TASK_STATUS_ON_REVIEW: queryset.filter(status=TASK_STATUS_ON_REVIEW).count(),
        #     TASK_STATUS_PROCESSING: queryset.filter(status=TASK_STATUS_PROCESSING).count(),
        #     "expired": queryset.filter(
        #         Q(status__in=[TASK_STATUS_PROCESSING, TASK_STATUS_ON_REVIEW], deadline__lt=now()) |
        #         Q(status=TASK_STATUS_COMPLETED, completed_time__isnull=False, completed_time__gt=F('deadline'))
        #     ).count()
        # })