import requests
from datetime import datetime

from django.db.models.functions import Lower
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework.decorators import action

from shared.permissions.data_management_permission import DataManagementPermission
from shared.constants.data_management import *
from shared.utils.apps import now
from shared.constants.tasks import *
from v1_0.models.data_service import DataService
from v1_0.models.tasks import Task
from v1_0.serializers.tasks import TaskSerializer, UpdateTaskSerializer
from v1_0.views.app_general_view import MSBaseView


class DataManagementViewSet(MSBaseView):
    permission_classes = (DataManagementPermission, )
    lookup_value_regex = r'[0-9a-z\-]+'
    http_method_names = ['head', 'options', 'get']

    def get_serializer_class(self):
        return super(DataManagementViewSet, self).get_serializer_class()

    @action(methods=["get"], detail=False)
    def get_task_by_user_id(self, request, *args, **kwargs):
        results = []
        user_id = kwargs.get("user_id")
        headers = {
            'Content-Type': 'application/json'
        }

        # Get task from group 6:
        url_6 = BASE_API_PROJECT_TASK + "/v1/tasks?user_id={}".format(user_id)
        res_6 = requests.get(url=url_6, headers=headers)
        if res_6.status_code in [200, 201]:
            results_6 = res_6.json().get("results")
            for result_6 in results_6:
                if result_6.get("deadline") is None:
                    deadline = None
                else:
                    deadline = datetime.utcfromtimestamp(result_6.get("deadline")).strftime('%Y-%m-%d %H:%M:%S')
                results.append({
                    "id": result_6.get("id"),
                    "name": result_6.get("name"),
                    "description": result_6.get("description"),
                    "percent_complete": result_6.get("percent_complete"),
                    "status": result_6.get("status"),
                    "deadline": deadline
                })

        # Get task by user id from group 12
        url_12 = BASE_API_PROCEDURE_TASK + "/api/procedure-task/implemented-by-user/{}".format(user_id)
        res_12 = requests.get(url=url_12, headers=headers)
        if res_12.status_code in [200, 201]:
            results_12 = res_12.json()
            for result_12 in results_12:
                results.append({
                    "id": result_12.get("id"),
                    "name": result_12.get("name"),
                    "description": result_12.get("content"),
                    "percent_complete": float(result_12.get("amount_of_accomplished_work")/result_12.get("amount_of_work")) * 100,
                    "status": "completed" if result_12.get("status") == 1 else "processing",
                    "deadline": result_12.get("deadline")
                })

        return Response(status=200, data=results)

    @action(methods=["get"], detail=False)
    def statistics(self, request, *args, **kwargs):
        from_param = self.request.query_params.get("from", None)
        to_param = self.request.query_params.get("to", None)
        statistic_result = DataService.statistics()
        return Response(status=200, data=statistic_result)

    @action(methods=["get"], detail=False)
    def top_users(self, request, *args, **kwargs):
        from_param = self.request.query_params.get("from", None)
        to_param = self.request.query_params.get("to", None)
        top = DataService(from_time=from_param, to_time=to_param).get_top(top_number=5)
        return Response(status=200, data=top)
