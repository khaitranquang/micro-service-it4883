from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, NotFound

from shared.permissions.attachment_permission import AttachmentPermission
from shared.attachments.aws_s3 import *
from v1_0.models.tasks import Task
from v1_0.models.task_attachments import TaskAttachment
from v1_0.serializers.attachments import UploadAttachmentSerializer, TaskAttachmentSerializer
from v1_0.views.app_general_view import MSBaseView


class AttachmentViewSet(MSBaseView):
    permission_classes = (AttachmentPermission, )
    http_method_names = ['head', 'options', 'post', 'delete']

    def get_object(self):
        try:
            attachment = TaskAttachment.objects.get(id=self.kwargs.get("pk"), task_id=self.kwargs.get("task_id"))
            self.check_object_permissions(request=self.request, obj=attachment)
            return attachment
        except TaskAttachment.DoesNotExist:
            raise NotFound

    def get_task(self):
        try:
            task = Task.objects.get(id=self.kwargs.get("task_id"))
            return task
        except Task.DoesNotExist:
            raise NotFound

    def get_serializer_class(self):
        if self.action == "create":
            return UploadAttachmentSerializer
        if self.action == "update_attachments":
            return TaskAttachmentSerializer
        return super(AttachmentViewSet, self).get_serializer_class()

    def create(self, request, *args, **kwargs):
        """
        This func to gen pre-signed url for uploading to AWS S3
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.save()

        upload_action = validated_data.get("action")
        file_name = validated_data.get("file_name")

        upload_data = dict()
        content_type = None
        limit_size = True
        extension = get_file_name_extension(key=file_name)[1]
        if extension.lower() in [".jpg", ".png", ".jpeg"]:
            content_type = 'image/jpeg'

        if upload_action in [UPLOAD_ACTION_TASK_ATTACHMENTS]:
            upload_data = {"file_name": file_name, "action": upload_action}

        file_utils = FileUtils(**upload_data)
        presigned_response = file_utils.gen_presigned_url(limit=limit_size, content_type=content_type)
        return Response(status=200, data=presigned_response)

    @action(methods=["post"], detail=False)
    def update_attachments(self, request, *args, **kwargs):
        task = self.get_task()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.save()
        attachments = validated_data.get("attachments", [])
        print(attachments)
        if attachments:
            TaskAttachment.create_multiple(task, *attachments)
        return Response(status=200, data={"success": True})

    def destroy(self, request, *args, **kwargs):
        """ This API Endpoint to delete attachment """
        attachment = self.get_object()
        # Delete on aws s3
        FileUtils.delete_file(url=attachment.url)
        attachment.delete()
        return Response(status=204)

