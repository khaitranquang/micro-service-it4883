from django.db.models import When, Q, Value, Case, IntegerField, F
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound

from shared.permissions.statistic_permission import StatisticPermission
from shared.utils.apps import now
from shared.constants.tasks import *
from shared.constants.projects import *
from v1_0.models.task_members import TaskMember
from v1_0.views.app_general_view import MSBaseView


class StatisticViewSet(MSBaseView):
    permission_classes = (StatisticPermission, )
    lookup_field = r'[0-9a-z\-]+'
    http_method_names = ['head', 'options', 'get']

    def get_serializer_class(self):
        return super(StatisticViewSet, self).get_serializer_class()

    @action(methods=["get"], detail=False)
    def user_statistic(self, request, *args, **kwargs):
        user_id = kwargs.get("user_id")
        time_now = now()

        # `When` query to calc deadline
        whens_deadline = [
            When(Q(task__deadline__isnull=True), then=Value(9999999999999999)),
            When(Q(task__deadline__isnull=False), then=F('task__deadline'))
        ]
        multiplier_case_deadline = Case(*whens_deadline, output_field=IntegerField(), default=Value(0))

        # Get number of tasks that user joined
        members = TaskMember.objects.filter(user_id=user_id).annotate(
            task_deadline=multiplier_case_deadline
        )
        return Response(status=200, data={
            "projects": {
                "total": members.values('task__project__id').distinct().count()
            },
            "tasks": {
                "total": members.count(),
                "status": {
                    TASK_STATUS_COMPLETED: members.filter(task__status=TASK_STATUS_COMPLETED).count(),
                    TASK_STATUS_PROCESSING: members.filter(task__status=TASK_STATUS_PROCESSING).count(),
                    TASK_STATUS_ON_REVIEW: members.filter(task__status=TASK_STATUS_ON_REVIEW).count()
                },
                "level": {
                    TASK_LEVEL_EASY: members.filter(task__level__gte=1, task__level__lt=4).count(),
                    TASK_LEVEL_MEDIUM: members.filter(task__level__gte=4, task__level__lt=8).count(),
                    TASK_LEVEL_DIFFICULT: members.filter(task__level__gte=8, task__level__lte=10).count()
                },
                "expired": members.filter(
                    Q(task__status__in=[TASK_STATUS_PROCESSING, TASK_STATUS_ON_REVIEW], task__deadline__lt=now()) |
                    Q(task__status=TASK_STATUS_COMPLETED, task__completed_time__isnull=False, task__completed_time__gt=F('task__deadline'))
                ).count()
            }
        })
