from rest_framework import serializers

from v1_0.models.api_log import ApiLog


class ApiLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApiLog
        fields = '__all__'

    def to_representation(self, instance):
        data = super(ApiLogSerializer, self).to_representation(instance)
        data["data_send"] = instance.get_data_send()
        return data