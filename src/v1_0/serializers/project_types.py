from rest_framework import serializers

from v1_0.models.project_type import ProjectType


class ProjectTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectType
        fields = ('id', 'name')
        read_only_fields = ('id', 'name')
