from rest_framework import serializers

from shared.constants.tasks import *
from shared.services.user_service import UserService
from v1_0.models.projects import Project
from v1_0.models.tasks import Task
from v1_0.serializers.projects import ProjectSerializer


class MemberTaskSerializer(serializers.Serializer):
    user_id = serializers.CharField()

    def validate(self, data):
        # Check user is existed?
        user_id = data.get("user_id")
        user = UserService.get_user_info(user_id=user_id)
        if user is None:
            raise serializers.ValidationError(detail={"user_id": ["This user does not exist"]})
        return data


class TaskSerializer(serializers.ModelSerializer):
    project = serializers.CharField()
    level = serializers.IntegerField(required=False)
    members = MemberTaskSerializer(many=True, required=False)

    class Meta:
        model = Task
        fields = ('id', 'name', 'description', 'created_time', 'updated_time', 'completed_time',
                  'status', 'deadline', 'label', 'level', 'project', 'members', 'percent_complete', )
        read_only_fields = ('id', 'created_time', 'updated_time', 'completed_time', 'percent_complete', )

    def validate(self, data):
        _deadline = data.get("deadline")
        if (_deadline is not None) and (_deadline <= 0):
            raise serializers.ValidationError(detail={"deadline": "Deadline is not valid"})

        _label = data.get("label", None)
        if (_label is not None) and (_label not in [TASK_LABEL_LOW, TASK_LABEL_MEDIUM, TASK_LABEL_HIGH]):
            raise serializers.ValidationError(detail={"label": "Label is not valid"})

        _level = data.get("level", None)
        if (_level is not None) and (_level < 1 or _level > 10):
            raise serializers.ValidationError(detail={"level": "level is not valid"})

        _members = data.get("members", None)

        if _members is not None:
            list_user_id = [member.get("user_id") for member in _members]
            new_list_user_id = [{"user_id": user_id} for user_id in list(set(list_user_id))]
            data["members"] = new_list_user_id

        return data

    def create(self, validated_data):
        project_id = validated_data.get("project")
        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            raise serializers.ValidationError(detail={"project": "Project does not exist"})
        validated_data["project"] = project
        new_task = Task.create(**validated_data)
        return new_task

    def to_representation(self, instance):
        data = super(TaskSerializer, self).to_representation(instance)
        data["project"] = instance.project.id

        if self.context.get('view').action == 'retrieve':
            data["attachments"] = instance.attachments.all().values('id', 'url')

        data["detail_project"] = ProjectSerializer(instance.project).data

        return data


class UpdateTaskSerializer(serializers.Serializer):
    status = serializers.CharField(required=False)
    label = serializers.CharField(required=False)
    level = serializers.IntegerField(required=False)
    name = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    deadline = serializers.IntegerField(required=False)
    percent_complete = serializers.FloatField(required=False)

    def validate(self, data):
        _status = data.get("status", None)
        if (_status is not None) and (_status not in [TASK_STATUS_PROCESSING, TASK_STATUS_COMPLETED]):
            raise serializers.ValidationError(detail={"status": "Status is not valid"})

        _label = data.get("label", None)
        if (_label is not None) and (_label not in [TASK_LABEL_LOW, TASK_LABEL_MEDIUM, TASK_LABEL_HIGH]):
            raise serializers.ValidationError(detail={"label": "Label is not valid"})

        _deadline = data.get("deadline", None)
        if (_deadline is not None) and (_deadline < 0):
            raise serializers.ValidationError(detail={"deadline": "Deadline is not valid"})

        _level = data.get("level", None)
        if (_level is not None) and (_level < 1 or _level > 10):
            raise serializers.ValidationError(detail={"level": "Level is not valid"})

        _percent_complete = data.get("percent_complete", None)
        if (_percent_complete is not None) and (_percent_complete < 0 or _percent_complete > 100):
            raise serializers.ValidationError(detail={"percent_complete": "Percent is not valid"})

        return data

    def save(self, **kwargs):
        return self.validated_data
