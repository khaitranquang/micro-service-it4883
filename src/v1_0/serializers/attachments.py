import re

from rest_framework import serializers

from shared.attachments.aws_s3 import *
from shared.utils.apps import check_url
from v1_0.models.task_attachments import TaskAttachment


class UploadAttachmentSerializer(serializers.Serializer):
    file_name = serializers.CharField(max_length=64)
    action = serializers.CharField(max_length=16)

    def validate(self, data):
        # Check action is valid ?
        _action = data.get("action")
        if _action not in LIST_UPLOAD_ACTION:
            raise serializers.ValidationError(detail={"action": ["Upload action không hợp lệ"]})

        # Handle file name
        regex_file_name = "^[A-Za-z0-9._()\- ]+$"
        _file_name = data.get("file_name")
        if not re.match(regex_file_name, _file_name):
            raise serializers.ValidationError(detail={"file_name": ["Tên file không hợp lệ"]})

        return data

    def save(self, **kwargs):
        return self.validated_data


class TaskAttachmentSerializer(serializers.Serializer):
    attachments = serializers.ListField(
        child=serializers.CharField(allow_blank=False),
        required=False,
        max_length=3
    )

    def validate(self, data):
        _attachments = data.get("attachments", None)
        if _attachments is not None:
            for attachment in _attachments:
                if check_url(url=attachment) is False:
                    raise serializers.ValidationError(detail={"attachments": ["Attachments is not valid url"]})
                if not attachment.startswith(
                        "https://{}.s3.amazonaws.com/bug-bounty".format(settings.AWS_S3_BUCKET)):
                    raise serializers.ValidationError(detail={"attachments": ["Attachments is not valid url"]})

        return data

    def save(self, **kwargs):
        validated_data = self.validated_data
        return validated_data
