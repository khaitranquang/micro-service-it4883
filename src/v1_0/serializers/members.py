from rest_framework import serializers

from shared.services.user_service import UserService
from v1_0.models.task_members import TaskMember


class MemberSerializer(serializers.Serializer):
    user_id = serializers.CharField()

    def validate(self, data):
        # Check user is existed?
        user_id = data.get("user_id")
        user = UserService.get_user_info(user_id=user_id)
        if user is None:
            raise serializers.ValidationError(detail={"user_id": ["This user does not exist"]})
        return data

    def save(self, **kwargs):
        task = kwargs.get("task")
        new_member = TaskMember.create(task=task, **self.validated_data)
        return new_member

    def to_representation(self, instance):
        data = super(MemberSerializer, self).to_representation(instance)
        data["id"] = instance.id
        data["user_id"] = instance.user_id
        data["task"] = instance.task_id
        data["access_time"] = instance.access_time
        data["role"] = "member" if instance.user_id != instance.task.project.created_by != instance.user_id else "owner"
        return data
