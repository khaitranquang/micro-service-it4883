from rest_framework import serializers

from shared.constants.projects import *
from shared.services.user_service import UserService
from v1_0.models.projects import Project


class ProjectSerializer(serializers.ModelSerializer):
    project_type = serializers.IntegerField()

    class Meta:
        model = Project
        fields = ('id', 'name', 'status', 'description', 'created_time', 'updated_time', 'created_by',
                  'completed_time', 'deadline', 'technique_index', 'project_type')
        read_only_fields = ('id', 'created_time', 'updated_time', 'status', 'completed_time', 'technique_index')
        
    def validate(self, data):
        # Check user is existed?
        _created_by = data.get("created_by")
        if _created_by is not None:
            user = UserService.get_user_info(user_id=_created_by)
            if user is None:
                raise serializers.ValidationError(detail={"created_by": ["This user does not exist"]})

        _deadline = data.get("deadline", None)
        if (_deadline is not None) and (_deadline <= 0):
            raise serializers.ValidationError(detail={"deadline": "Deadline is not valid"})

        _project_type = data.get("project_type", None)
        if _project_type is not None:
            if (_project_type < 0) or (_project_type > 3):
                raise serializers.ValidationError(detail={"project_type": ["Project type is not valid"]})

        _technique_index = data.get("technique_index", None)
        if _technique_index is not None:
            if (_technique_index < 1) or (_technique_index > 10):
                raise serializers.ValidationError(detail={"technique_index": ["Technique index is not valid"]})

        return data
    
    def create(self, validated_data):
        new_project = Project.create(**validated_data)
        return new_project
    
    def to_representation(self, instance):
        data = dict()
        data["id"] = instance.id
        data["name"] = instance.name
        data["status"] = instance.status
        data["description"] = instance.description
        data["created_time"] = instance.created_time
        data["updated_time"] = instance.updated_time
        data["completed_time"] = instance.completed_time
        data["created_by"] = instance.created_by
        data["deadline"] = instance.deadline
        data["technique_index"] = instance.technique_index
        data["project_type"] = instance.project_type.id
        return data


class UserProjectSerializer(ProjectSerializer):
    def to_representation(self, instance):
        user_id = self.context.get("user_id")
        print("User_id: ", user_id, type(user_id), instance.created_by, type(instance.created_by))
        data = super(UserProjectSerializer, self).to_representation(instance)
        data["role"] = "owner" if str(instance.created_by) == str(user_id) else "member"
        return data


class UpdateProjectSerializer(serializers.Serializer):
    name = serializers.CharField()
    description = serializers.CharField(allow_blank=True)
    deadline = serializers.IntegerField(required=False)
    status = serializers.CharField(required=False)
    technique_index = serializers.IntegerField(required=False)
    project_type = serializers.IntegerField(required=False)

    def validate(self, data):
        _deadline = data.get("deadline", None)
        if (_deadline is not None) and (_deadline < 0):
            raise serializers.ValidationError(detail={"deadline": ["Deadline is not valid"]})

        _status = data.get("status", None)
        if _status not in [PROJECT_STATUS_PROCESSING, PROJECT_STATUS_COMPLETED]:
            raise serializers.ValidationError(detail={"status": ["Status is not valid"]})

        _technique_index = data.get("technique_index", None)
        if _technique_index is not None:
            if (_technique_index < 1) or (_technique_index > 10):
                raise serializers.ValidationError(detail={"technique_index": ["Technique index is not valid"]})

        _project_type = data.get("project_type", None)
        if _project_type is not None:
            if (_project_type < 0) or (_project_type > 3):
                raise serializers.ValidationError(detail={"project_type": ["Project type is not valid"]})

        return data

    def save(self, **kwargs):
        return self.validated_data
