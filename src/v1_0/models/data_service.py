import requests
import time
import datetime

from django.core.cache import cache

from shared.constants.data_management import *


HEADERS = {
    'Content-Type': 'application/json'
}


class DataService:
    def __init__(self, from_time=None, to_time=None):
        self.__from_time = from_time
        self.__to_time = to_time
        # List user ids complete task before deadline
        self.__user_completed_deadline = []
        # List all tasks
        self.__tasks = self.__get_all_tasks()

    def get_user_completed_deadline(self):
        return self.__user_completed_deadline

    def get_tasks(self):
        return self.__tasks

    def __get_all_tasks(self):
        recurrent_tasks = self.get_recurrent_tasks()
        procedure_tasks = self.get_procedure_tasks()
        project_tasks = self.get_project_tasks()
        tasks = recurrent_tasks + project_tasks + procedure_tasks
        return tasks

    @staticmethod
    def convert_timestamp(string_time, task_type):
        if task_type == "recurrent_task":
            timestamp = time.mktime(datetime.datetime.strptime(string_time, "%Y-%m-%dT%H:%M:%S.%fZ").timetuple())
            return int(timestamp)
        elif task_type == "procedure_task":
            timestamp = time.mktime(datetime.datetime.strptime(string_time, "%Y-%m-%d %H:%M:%S").timetuple())
            return int(timestamp)

    @staticmethod
    def convert_from_timestamp(timestamp, task_type):
        try:
            if task_type == "recurrent_task":
                return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            elif task_type == "procedure_task":
                return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        except:
            return None

    def get_recurrent_tasks(self):
        recurrent_tasks = []
        url = BASE_API_RECURRENT_TASK + "/api/recurrent-tasks?"
        if self.__from_time is not None:
            _start = self.convert_from_timestamp(timestamp=self.__from_time, task_type="recurrent_task")
            if _start is not None:
                url += "&start={}".format(_start)
        if self.__to_time is not None:
            _finish = self.convert_from_timestamp(timestamp=self.__to_time, task_type="recurrent_task")
            if _finish is not None:
                url += "&finish={}".format(_finish)

        res = requests.get(url=url, headers=HEADERS)
        if res.status_code == 200:
            results = res.json()
            for task in results:
                if task.get("percentComplete") == 100:
                    self.__user_completed_deadline += [doer["id"] for doer in task.get("coDoers")]
                recurrent_tasks.append({
                    "id": task.get("_id"),
                    "type": "recurrent_task",
                    "name": task.get("name"),
                    "percent_completed": task.get("percentComplete"),
                    "created_time": self.convert_timestamp(task.get("start"), "recurrent_task"),
                    "deadline": self.convert_timestamp(task.get("due"), "recurrent_task"),
                    "completed_time": None
                })
        return recurrent_tasks
        
    def get_procedure_tasks(self):
        procedure_tasks = []
        if cache.get("procedure_tasks") is None:
            url = BASE_API_PROCEDURE_TASK + "/api/procedure-task"
            res = requests.get(url=url, headers=HEADERS)
            if res.status_code == 200:
                results = res.json()
                for task in results:
                    percent_complete = float(task.get("amount_of_accomplished_work") / task.get("amount_of_work")) * 100

                    procedure_tasks.append({
                        "id": task.get("id"),
                        "implementer": task.get("implementer"),
                        "type": "procedure_task",
                        "name": task.get("name"),
                        "percent_completed": percent_complete,
                        "created_time": self.convert_timestamp(task.get("created_at"), "procedure_task"),
                        "deadline": self.convert_timestamp(task.get("deadline"), "procedure_task"),
                        "completed_time": None
                    })
                cache.set('procedure_tasks', procedure_tasks, 86400*7)
        else:
            procedure_tasks = cache.get("procedure_tasks")
        for task in procedure_tasks:
            if task.get("percent_completed") == 100:
                self.__user_completed_deadline += [str(task.get("implementer"))]
        return procedure_tasks

    def get_project_tasks(self):
        project_tasks = []
        url = BASE_API_PROJECT_TASK + "/v1/tasks?"
        if self.__from_time is not None:
            url += "&from={}".format(self.__from_time)
        if self.__to_time is not None:
            url += "&to={}".format(self.__to_time)

        res = requests.get(url=url, headers=HEADERS)
        if res.status_code == 200:
            results = res.json().get("results")
            for task in results:
                percent_completed = task.get("percent_complete")
                if percent_completed == 100:
                    self.__user_completed_deadline += [member.get("user_id") for member in task.get("members")]
                project_tasks.append({
                    "id": task.get("id"),
                    "type": "project_task",
                    "name": task.get("name"),
                    "percent_completed": percent_completed,
                    "created_time": task.get("created_time"),
                    "deadline": task.get("deadline"),
                    "completed_time": task.get("completed_time")
                })
        return project_tasks

    def get_top(self, top_number):
        top = []
        user_ids = self.__user_completed_deadline
        users_sorted = sorted(user_ids, key=user_ids.count, reverse=True)
        users_top = sorted(set(users_sorted), key=users_sorted.index)

        for user_top in users_top:
            if len(top) < top_number:
                top.append({
                    "id": user_top,
                    "number_tasks": users_sorted.count(user_top)
                })
            else:
                break
        return top

    @classmethod
    def statistics(cls):
        completed = 0
        processing = 0
        expired = 0
        recurrent_statistic_url = BASE_API_RECURRENT_TASK + "/api/recurrent-tasks/statistics?"
        procedure_statistic_url = BASE_API_PROCEDURE_TASK + "/api/analyze-procedure-task?"
        project_statistic_url = BASE_API_PROJECT_TASK + "/v1/tasks/statistics?"

        res_recurrent = requests.get(url=recurrent_statistic_url, headers=HEADERS)
        print("Recurrent statistic: ", res_recurrent.status_code)
        if res_recurrent.status_code == 200:
            result_recurrent = res_recurrent.json()
            completed += result_recurrent.get("finished").get("count", 0)
            processing += result_recurrent.get("doing").get("count", 0)
            expired += result_recurrent.get("overdue").get("count", 0)

        print("Cache: ", cache.get('procedure_statistic'))
        if cache.get('procedure_statistic') is None:
            res_procedure = requests.get(url=procedure_statistic_url, headers=HEADERS)
            print("Procedure statistic: ", res_procedure.status_code)
            if res_procedure.status_code == 200:
                result_procedure = res_procedure.json()
                completed += result_procedure.get("num_finished_task", 0)
                processing += result_procedure.get("num_unfinished_task", 0)
                expired += result_procedure.get("num_overdue_task", 0)
                cache.set('procedure_statistic', result_procedure, 86400*7)
        else:
            procedure_statistic = cache.get('procedure_statistic')
            completed += procedure_statistic.get("num_finished_task", 0)
            processing += procedure_statistic.get("num_unfinished_task", 0)
            expired += procedure_statistic.get("num_overdue_task", 0)

        res_project = requests.get(url=project_statistic_url, headers=HEADERS)
        print("Project statistic: ", res_project.status_code)
        if res_project.status_code == 200:
            result_project = res_project.json()
            completed += (result_project.get("completed_not_expired", 0) + result_project.get("completed_expired", 0))
            processing += result_project.get("processing_not_expired", 0)
            expired += result_project.get("processing_expired", 0)

        total = completed + processing + expired
        return {
            "total": total,
            "completed": completed,
            "processing": processing,
            "expired": expired
        }
