import uuid

from django.db import models

from shared.constants.tasks import *
from shared.utils.apps import now
from shared.constants.tasks import TASK_STATUS_PROCESSING
from v1_0.models.projects import Project


class Task(models.Model):
    id = models.CharField(primary_key=True, max_length=128, default=uuid.uuid4)
    name = models.CharField(max_length=255)
    description = models.TextField(default="")
    created_time = models.IntegerField()
    updated_time = models.IntegerField(null=True)
    percent_complete = models.FloatField(default=0)
    completed_time = models.IntegerField(null=True)
    status = models.CharField(max_length=128, default=TASK_STATUS_PROCESSING)
    deadline = models.IntegerField(null=True)
    label = models.CharField(max_length=16, default="", blank=True)
    # level = models.CharField(max_length=128, default=TASK_LEVEL_MEDIUM)
    level = models.IntegerField(default=1)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="tasks")

    class Meta:
        db_table = 'tasks'

    @classmethod
    def create(cls, **data):
        _project = data.get("project")
        _name = data.get("name")
        _description = data.get("description", "")
        _label = data.get("label", "")
        _level = data.get("level", 1)
        _deadline = data.get("deadline", None)

        _members = data.get("members", None)
        print(_members)

        new_task = cls(
            name=_name, description=_description, created_time=now(),
            project=_project, deadline=_deadline, label=_label, level=_level
        )
        new_task.save()
        if _members is not None:
            from v1_0.models.task_members import TaskMember
            TaskMember.create_multiple(task=new_task, members=_members)
        return new_task
