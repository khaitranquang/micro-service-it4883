import uuid
import ast

from django.db import models

from shared.utils.apps import now


class ApiLog(models.Model):
    id = models.CharField(primary_key=True, max_length=128, default=uuid.uuid4)
    ip = models.CharField(max_length=128, default="")
    object_id = models.CharField(max_length=128, default=None, null=True)
    created_time = models.IntegerField()
    method = models.CharField(max_length=128)
    path = models.TextField()
    data_send = models.TextField(default=None, null=True)
    response_code = models.IntegerField()
    type = models.CharField(max_length=128)

    @classmethod
    def create(cls, **data):
        ip = data.get("ip")
        method = data.get("method")
        path = data.get("path")
        data_send = data.get("data_send")
        response_code = data.get("response_code", 200)
        api_type = data.get("type")
        object_id = data.get("object_id")
        new_log = cls(
            ip=ip, created_time=now(), method=method, path=path, data_send=data_send, response_code=response_code,
            type=api_type, object_id=object_id
        )
        new_log.save()
        return new_log

    def get_data_send(self):
        if self.data_send is None or self.data_send == "":
            return dict()
        try:
            return ast.literal_eval(self.data_send)
        except Exception as e:
            print("Get data send log error: {}".format(e))
            return dict()