from django.db import models

from shared.utils.apps import now
from v1_0.models.tasks import Task


class TaskMember(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.CharField(max_length=255)
    access_time = models.IntegerField()
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="members")

    class Meta:
        db_table = 'task_members'
        unique_together = ('user_id', 'task')

    @classmethod
    def create(cls, task, **data):
        user_id = data.get("user_id")
        new_member = cls(user_id=user_id, access_time=now(), task=task)
        new_member.save()
        return new_member

    @classmethod
    def create_multiple(cls, task, members):
        list_members_obj = []
        for member in members:
            list_members_obj.append(cls(user_id=member.get("user_id"), access_time=now(), task=task))
        cls.objects.bulk_create(list_members_obj)