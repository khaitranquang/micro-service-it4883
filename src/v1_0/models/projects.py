import uuid

from django.db import models

from shared.constants.projects import PROJECT_STATUS_PROCESSING
from shared.utils.apps import now

from v1_0.models.project_type import ProjectType


class Project(models.Model):
    id = models.CharField(primary_key=True, max_length=128, default=uuid.uuid4)
    name = models.CharField(max_length=255)
    # project_type = models.IntegerField(default=0)
    description = models.TextField(default="", blank=True)
    created_time = models.IntegerField()
    updated_time = models.IntegerField(null=True)
    completed_time = models.IntegerField(null=True)
    created_by = models.CharField(max_length=128)
    deadline = models.IntegerField(null=True)
    status = models.CharField(max_length=64, default=PROJECT_STATUS_PROCESSING)
    technique_index = models.IntegerField(default=1)
    project_type = models.ForeignKey(ProjectType, on_delete=models.CASCADE, related_name="projects")

    class Meta:
        db_table = 'projects'

    @classmethod
    def create(cls, **data):
        _name = data.get("name")
        _description = data.get("description", "")
        _created_by = data.get("created_by", "")
        _deadline = data.get("deadline", None)
        _project_type = data.get("project_type", 0)

        new_project = cls(
            name=_name, description=_description, created_by=_created_by, created_time=now(), deadline=_deadline,
            project_type_id=_project_type
        )
        new_project.save()
        return new_project
