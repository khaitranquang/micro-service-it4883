from django.db import models

from shared.attachments.aws_s3 import FileUtils, UPLOAD_ACTION_TASK_ATTACHMENTS
from shared.log.cylog import CyLog
from shared.utils.apps import url_decode
from v1_0.models.tasks import Task


class TaskAttachment(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.TextField()
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="attachments")

    class Meta:
        db_table = 'task_attachments'

    @classmethod
    def create_multiple(cls, task, *attachments ):
        for attachment in attachments:
            try:
                attachment_url = url_decode(attachment)
                new_url = FileUtils.get_s3_url(temp_url=attachment_url, upload_action=UPLOAD_ACTION_TASK_ATTACHMENTS)
                # File is not exists => Continue
                if new_url != "":
                    new_attachment = cls(url=new_url, task=task)
                    new_attachment.save()
            except Exception as e:
                CyLog.debug(
                    **{"message": "Copy file task {}  error: {}".format(task.id, e)})
                continue