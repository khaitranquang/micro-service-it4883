from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

# from shared.constants.cache import LONG_TIME_CACHE
from v1_0 import views


router = DefaultRouter(trailing_slash=False)
router.register(r'projects', views.ProjectViewSet, 'projects')
router.register(r'tasks', views.TaskViewSet, 'tasks')
router.register(r'project_types', views.ProjectTypeViewSet, 'project_types')

urlpatterns = [
    url(r'^', include(router.urls)),
]

urlpatterns += [
    url(r'^logs$', views.ApiLogViewSet.as_view({'get': 'list'})),
]

urlpatterns += [
    url(r'^data_management/get_tasks_by_user_id/(?P<user_id>[0-9a-z\-\_]+)$', views.DataManagementViewSet.as_view({'get': 'get_task_by_user_id'})),
    url(r'^data_management/tasks/statistics$', views.DataManagementViewSet.as_view({'get': 'statistics'})),
    url(r'^data_management/tasks/statistics/top_users$', views.DataManagementViewSet.as_view({'get': 'top_users'})),
]


urlpatterns += [
    url(r'^users/(?P<user_id>[0-9a-z\-\_]+)/projects$', views.ProjectViewSet.as_view({'get': 'user_projects'})),
]

urlpatterns += [
    url(r'^tasks/(?P<task_id>[0-9a-z\-]+)/members$', views.TaskMemberViewSet.as_view({'get': 'list', 'post': 'create'})),
    url(r'^tasks/(?P<task_id>[0-9a-z\-]+)/members/(?P<pk>[0-9]+)$', views.TaskMemberViewSet.as_view({'delete': 'destroy'})),
]

urlpatterns += [
    url(r'^attachments$', views.AttachmentViewSet.as_view({'post': 'create'})),
    url(r'^tasks/(?P<task_id>[0-9a-z\-]+)/attachments$', views.AttachmentViewSet.as_view({'post': 'update_attachments'})),
    url(r'^tasks/(?P<task_id>[0-9a-z\-]+)/attachments/(?P<pk>[0-9]+)$', views.AttachmentViewSet.as_view({'delete': 'destroy'})),

]

urlpatterns += [
    url(r'^statistics/users/(?P<user_id>[0-9a-z\-\_A-Z]+)$', views.StatisticViewSet.as_view({'get': 'user_statistic'})),
]